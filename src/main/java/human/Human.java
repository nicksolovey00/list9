package human;

import java.util.Objects;

public class Human {
    Sex sex;
    int age;
    String firstName;
    String secondName;
    String middleName;


    public Human(Sex sex, String firstName, String secondName, String middleName, int age) {
        this.age = age;
        this.sex = sex;
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }

    public Human(Human other){
        this.sex = other.sex;
        this.age = other.age;
        this.firstName = other.firstName;
        this.secondName = other.secondName;
        this.middleName = other.middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Human incrementAge(Human human){
        Human newHuman = new Human(human);
        newHuman.age++;
        return newHuman;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                sex == human.sex &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(secondName, human.secondName) &&
                Objects.equals(middleName, human.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sex, age, firstName, secondName, middleName);
    }

    @Override
    public String toString() {
        return "Human{" +
                "sex=" + sex +
                ", age=" + age +
                ", firstName='" + firstName + '\'' +
                '}';
    }

    public static int compareBySex(Human that, Human other){
        if(that.sex.equals(Sex.Female) && other.sex.equals(Sex.Male)){
            return 1;
        }
        else if (that.sex.equals(Sex.Male) && other.sex.equals(Sex.Female)){
            return -1;
        }
        else return 0;
    }
}
