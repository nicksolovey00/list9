package human;

public class Student extends Human {
    String university;
    String faculty;
    String specialty;

    public Student(Human human, String university, String faculty, String specialty){
        super(human);
        this.university = university;
        this.faculty = faculty;
        this.specialty = specialty;
    }

    public Student(Sex sex, String firstName, String secondName, String middleName, int age, String university, String faculty, String specialty) {
        super(sex, firstName, secondName, middleName, age);
        this.university = university;
        this.faculty = faculty;
        this.specialty = specialty;
    }
}
