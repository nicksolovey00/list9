import human.Human;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;

public class LambdaDemo {
    public static final Function<String, Integer> getLength = String::length;

    /* Филиппов А.В. 29.11.2020 Комментарий не удалять.
     Не работает. см. тест
    */
    public static final Function<String, Character> getFirstSymbol = string -> (string == null || "".equals(string))? null: string.charAt(0);
    public static final Function<String, Boolean> doesStringContainSpaces = string -> {
        if ("".equals(string)) {
            return false;
        }
        return string.indexOf(' ') > -1;
    };
    /* Филиппов А.В. 29.11.2020 Комментарий не удалять.
     Не работает. См. тест. Пустая строка - это не слово.
    */
    public static final Function<String, Integer> amountOfWordsInString = string -> {
        if(string == null || string.equals("")){
            return 0;
        }
        int count = 0;
        for(String s: string.split(",")){
            if(!Pattern.matches("\\s*", s)){
                count++;
            }
        }
        return count;
    };
    public static final Function<Human, Integer> getHumanAge = Human::getAge;
    public static final BiFunction<Human, Human, Boolean> isSecondNamesEqual = (firstHuman, secondHuman) -> firstHuman.getSecondName().equals(secondHuman.getSecondName());
    public static final Function<Human, String> getFullName = human -> human.getFirstName() + " " + human.getSecondName() + " " + human.getMiddleName();
    /* Филиппов А.В. 29.11.2020 Комментарий не удалять.
     Если каждый метод пихать в класс, то он распухнет.
     new Human(human).setAge(human.getAge() + 1);
     Хорошо, приму к сведению
    */
    public static final Function<Human, Human> makeHumanOneYearOlder = human -> human.incrementAge(human);
    public static final OperationWithThreePeople<Boolean> checkAllYounger = ((firstHuman, secondHuman, thirdHuman, maxAge) ->
            firstHuman.getAge() < maxAge && secondHuman.getAge() < maxAge && thirdHuman.getAge() < maxAge);
}

interface OperationWithThreePeople<T>{
    T calculate(Human firstHuman, Human secondHuman, Human thirdHuman, int maxAge);
}

