import human.Human;
import human.Sex;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

public class TestStreamAPIDemo {
    @Test
    public void testDeleteAllNullObjects(){
        List<Object> list= new ArrayList<>();
        list.add(null);
        list.add(Sex.Female);
        list.add(null);
        list.add(Sex.Male);

        List<Object> exp = new ArrayList<>();
        Collections.addAll(exp, Sex.Female, Sex.Male);

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.deleteAllNullObjects, list));
    }

    @Test
    public void testFindAmountOfPositiveNumbers(){
        Set<Integer> set= new HashSet<>();
        set.add(-1);
        set.add(7);
        set.add(6);
        set.add(3);

        Assert.assertEquals(3, (int)LambdaRunner.run(StreamApiDemo.findAmountOfPositiveNumbers, set));
    }

    @Test
    public void testFindFirstEven(){
        List<Integer> list1 = new ArrayList<>();
        Collections.addAll(list1, 1, 88, 1, 3, 4, 7, 9);
        Assert.assertEquals(88, (int)LambdaRunner.run(StreamApiDemo.findFirstEven, list1));

        List<Integer> list2 = new ArrayList<>();
        Collections.addAll(list2, 1, 3, 5, 7);
        Assert.assertNull(LambdaRunner.run(StreamApiDemo.findFirstEven, list2));
    }

    @Test
    public void testGetSquaredList(){
        Integer[] array = new Integer[]{
                3, 3, 4, 1, 2, 1
        };
        List<Integer> exp = new ArrayList<>();
        Collections.addAll(exp, 9, 16, 1, 4);

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.getSquaredList, array));
    }

    @Test
    public void testRemoveAllEmptyStringsAndSort(){
        List<String> list = new ArrayList<>();
        Collections.addAll(list, "a4", "", "", "a2", "a3", "a1");

        List<String> exp = new ArrayList<>();
        Collections.addAll(exp, "a1", "a2", "a3", "a4");

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.removeAllEmptyStringsAndSort, list));
        for(String s: list){
            System.out.println(s);
        }
    }

    @Test
    public void testSortStringSetReversedOrder(){
        Set<String> set = new HashSet<>();
        Collections.addAll(set, "a3", "a1", "a4", "a2");

        List<String> exp = new ArrayList<>();
        Collections.addAll(exp, "a4", "a3", "a2", "a1");

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.sortStringSetReversedOrder, set));
    }

    @Test
    public void testGetSumOfSquaredElements(){
        Set<Integer> set = new HashSet<>();
        Collections.addAll(set, 1, 2, 3, 4);
        Assert.assertEquals(30, (int)LambdaRunner.run(StreamApiDemo.getSumOfSquaredElements, set));
    }

    @Test
    public void testGetMaxAge(){
        List<Human> humans = new ArrayList<>();
        humans.add(new Human(Sex.Female, "", "", "", 33));
        humans.add(new Human(Sex.Female, "", "", "", 44));
        humans.add(new Human(Sex.Female, "", "", "", 55));
        humans.add(new Human(Sex.Female, "", "", "", 22));
        humans.add(new Human(Sex.Female, "", "", "", 11));

        Assert.assertEquals(55, (int)LambdaRunner.run(StreamApiDemo.getMaxAge, humans));
    }

    @Test
    public void testSortHumans(){
        List<Human> humans = new ArrayList<>();
        humans.add(new Human(Sex.Female, "", "", "", 33));
        humans.add(new Human(Sex.Male, "", "", "", 44));
        humans.add(new Human(Sex.Male, "", "", "", 55));
        humans.add(new Human(Sex.Female, "", "", "", 22));
        humans.add(new Human(Sex.Male, "", "", "", 11));

        humans = (List<Human>) LambdaRunner.run(StreamApiDemo.sortHumansBySex, humans);
    }
}
